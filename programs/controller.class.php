<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor\Ctrl;


class Controller extends \bab_Controller
{
    /**
     * (non-PHPdoc)
     * @see \bab_Controller::getControllerTg()
     */
    protected function getControllerTg()
    {
        return 'addon/projecteditor/main';
    }

    
    /**
     * @return CtrlProject
     */
    public function project($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/project.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'Project', $proxy);
    }
    
    
    /**
     * @return CtrlResource
     */
    public function resource($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/resource.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'Resource', $proxy);
    }
    
    
    /**
     * @return CtrlTask
     */
    public function task($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/task.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'Task', $proxy);
    }
    
    /**
     * @return CtrlAssignment
     */
    public function assignment($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/assignment.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'Assignment', $proxy);
    }


    /**
     * @return CtrlGantt
     */
    public function gantt($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/gantt.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'Gantt', $proxy);
    }
}
