; <?php
// @codingStandardsIgnoreStart
/*
[general]
name                            ="projecteditor"
version	                        ="0.1"
addon_type                      ="EXTENSION"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
description                     ="Project editor"
description.fr                  ="Editeur de projet"
delete                          =1
ov_version                      ="8.1.0"
php_version                     ="5.3.0"
author                          ="Cantico"
addon_access_control            ="0"
icon							="project-plan.png"

[addons]

jquery=">=1.4.4"
widgets=">=1.0.35"

;*/
// @codingStandardsIgnoreStart
?>