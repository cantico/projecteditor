<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Task;

class TaskFullFrame extends UiFrame
{
    /**
     * @var Task
     */
    protected $task;
    
    public function __construct(Task $Task)
    {
        $this->task = $Task;
        
        parent::__construct(null, bab_Widgets()->VBoxLayout()->setVerticalSpacing(.8, 'em'));
        
        $this->loadItems();
        
        $this->addClass('task-fullframe');
    }
    
    
    /**
     * @return Task
     */
    protected function getRecord()
    {
        return $this->task;
    }

    
    protected function loadItems()
    {
        $set = $this->task->getParentSet();
        
        $this->addItem($this->labeled(translate('Type'), $set->Type));
        
        if ($alerts = $this->alerts()) {
            $this->addItem($alerts);
        }
        
        $this->addItem($this->labeled(translate('Scheduled start'), $set->Start));
        $this->addItem($this->labeled(translate('Scheduled finish'), $set->Finish));

        $this->addItem($this->labeled(translate('Actual start'), $set->ActualStart));
        $this->addItem($this->labeled(translate('Actual finish'), $set->ActualFinish));
        
        
        
        $this->addItem($this->constraint());
        $this->addItem($this->labeled(translate('Critical'), $set->Critical));
        $this->addItem($this->labeled(translate('Deadline'), $set->Deadline));
        $this->addItem($this->labeled(translate('Effort driven'), $set->EffortDriven));
        $this->addItem($this->labeled(translate('Estimated'), $set->Estimated));

        $this->addItem($this->duration());
        $this->addItem($this->work());
        
        
        
        $this->addItem($this->labeled(translate('Early start'), $set->EarlyStart));
        $this->addItem($this->labeled(translate('Late start'), $set->LateStart));
        $this->addItem($this->labeled(translate('Early finish'), $set->EarlyFinish));
        $this->addItem($this->labeled(translate('Late finish'), $set->LateFinish));
        
        $this->addItem($this->labeled(translate('Ignore resource calendar'), $set->IgnoreResourceCalendar));
        $this->addItem($this->labeled(translate('Hyperlink'), $set->Hyperlink));
        $this->addItem($this->labeled(translate('Hyperlink address'), $set->HyperlinkAddress));
        

        $this->addItem($this->labeled(translate('Recurring'), $set->Recurring));
        $this->addItem($this->labeled(translate('Summary'), $set->Summary));
        
        
        $this->addItem($this->assignments());
    }
    
    
    
    
    protected function assignments()
    {
        $widgets = \bab_Widgets();
        $list = UiLoader::assignmentList($this->task);
        
        $layout = $widgets->HBoxLayout();
        
        $frame = $widgets->Frame(null, $layout);
        $frame->addClass(\Func_Icons::ICON_LEFT_24);
        $frame->addClass('widget-100pc');
        
        $frame->addItem(
            $widgets->Title(translate('Assignments'))->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
        );
        
        $frame->addItem(
            $widgets->Link(
                $widgets->Icon(translate('Add'), \Func_Icons::ACTIONS_LIST_ADD),
                controller()->assignment()->edit(
                    array(
                        'TaskUID' => $this->task->UID,
                        'project' => $this->task->getFkPk('project')
                    )
                )
            )
        );
        
        return $widgets->VBoxItems($frame, $list);
    }
    
    
    
    /**
     * 
     * 
     */
    protected function alerts()
    {
        $widgets = \bab_Widgets();
        $layout = $widgets->FlowLayout()->setSpacing(2, 'em');
        
        $frame = $widgets->Frame(null, $layout);
        $frame->addClass(\Func_Icons::ICON_LEFT_24);
        
        if ($active = $this->active()) {
            $frame->addItem($active);
        }
        
        if ($milestone = $this->milestone()) {
            $frame->addItem($milestone);
        }
        
        if (0 === count($layout->getItems())) {
            return null;
        }
        
        return $frame;
    }
    
    
    protected function active()
    {
        if ($this->task->Active) {
            return null;
        }
        
        $widgets = \bab_Widgets();
        return $widgets->Icon(translate('This task is disabled'), \Func_Icons::STATUS_DIALOG_WARNING);
    }
    
    
    protected function milestone()
    {
        if (!$this->task->Milestone) {
            return null;
        }
         
        $widgets = \bab_Widgets();
        return $widgets->Icon(translate('This task is a milestone'), \Func_Icons::STATUS_DIALOG_INFORMATION);
    }
    
    
    
    /**
     * ConstraintType & ConstraintDate
     */
    protected function constraint()
    {
        $set = $this->task->getParentSet();
        
        $constraintType = $set->ConstraintType->output($this->task->ConstraintType);
        $constraintDate = $set->ConstraintDate->output($this->task->ConstraintDate);
        
        $constraint = $constraintType;
        
        if ($constraintDate) {
            $constraint .= ' '.$constraintDate;
        }
        
        return $this->text(translate('Constraint'), $constraint);
    }
    
    
    /**
     * Display information about task duration
     * 
     * Duration
     * ActualDuration
     * RemainingDuration
     */
    protected function duration()
    {
        $widgets = \bab_Widgets();
        
        $task = $this->task;
        $set = $task->getParentSet();
        
        $planned = new Duration($set->getInterval($task->Duration));
        $actual = new Duration($set->getInterval($task->ActualDuration));
        $remaining = new Duration($set->getInterval($task->RemainingDuration));
        
        $layout = $widgets->FlowLayout()->setSpacing(2, 'em');
        
        if (!$planned->isEmpty()) {
            $layout->addItem($this->text(translate('Planned duration'), (string) $planned));
        }
        
        if (!$actual->isEmpty()) {
            
            // The Actual Duration field shows the span of actual working time for a
            // task so far, based on the scheduled duration and current remaining work
            // or percent complete.
            
            $description = translate(
                'span of actual working time based on the scheduled duration and current remaining work'
            );
            
            $layout->addItem(
                $widgets->VBoxItems(
                    $this->text(translate('Actual duration'), (string) $actual),
                    $widgets->Label($description)->addClass('widget-small')
                )
            );
        }
        
        $percentComplete = (int) $task->PercentWorkComplete;
        $completeStr = sprintf(translate('%d%% completed'), $percentComplete);
        
        
        if (!$remaining->isEmpty()) {
            
            // The Remaining Duration field shows the amount of time required
            // to complete the unfinished portion of a task.
            
            // Remaining Duration = Duration - Actual Duration
            // Remaining Duration = Duration - (Duration * Percent Complete)
            
            $progress = (string) $remaining;
            $progress .= ' ('.$completeStr.')';
            
            $layout->addItem($this->text(translate('Remaining duration'), $progress));
        } elseif ($percentComplete < 100) {
            
            $layout->addItem($widgets->Label($completeStr));
            
        } else {
            
            $layout->addItem($widgets->Label(translate('This task duration is completed')));
        }
        
        
        
        return $widgets->Section(translate('Duration'), $layout);
    }
    
    
    
    /**
     * Display information about workload of task
     * 
     * Work
     * Regular work
     * Actual overtime work
     * Remaining work
     * Percent work complete
     */
    protected function work()
    {
        $widgets = \bab_Widgets();
        
        $task = $this->task;
        $set = $this->task->getParentSet();

        $actual = new Duration($set->getInterval($task->ActualWork));
        $scheduled = new Duration($set->getInterval($task->Work));
        $regular = new Duration($set->getInterval($task->RegularWork));
        $overtime = new Duration($set->getInterval($task->OvertimeWork));
        $actualovertime = new Duration($set->getInterval($task->ActualOvertimeWork));
        $remaining = new Duration($set->getInterval($task->RemainingWork));
        
        $percentComplete = (int) $task->PercentWorkComplete;
        
        $layout = $widgets->FlowLayout()->setSpacing(2, 'em');
        
        if (!$actual->isEmpty()) {
            $layout->addItem($this->text(translate('Actual'), (string) $actual));
        } else {
            $layout->addItem($widgets->Label(translate('No work duration on this task')));
        }
        
        if (!$scheduled->isEmpty()) {
            $layout->addItem($this->text(translate('Scheduled'), (string) $scheduled));
        }
        
        if (!$regular->isEmpty()) {
            $layout->addItem($this->text(translate('Regular'), (string) $regular));
        }
        
        if (!$overtime->isEmpty()) {
            $layout->addItem($this->text(translate('Overtime'), (string) $overtime));
        }
        
        if (!$actualovertime->isEmpty()) {
            $layout->addItem(
                $this->text(
                    translate('Actual overtime'),
                    (string) $actualovertime
                )
            );
        }
        
        $completeStr = sprintf(translate('%d%% completed'), $percentComplete);
        
        if (!$remaining->isEmpty()) {
            
            // Remaining Work = Work - Actual Work
            
            $progress = (string) $remaining;
            $progress .= ' ('.$completeStr.')';
            
            $layout->addItem($this->text(translate('Remaining'), $progress));
        } elseif ($percentComplete < 100) {
            
            $layout->addItem($widgets->Label($completeStr));
        } else {
            
            $layout->addItem($widgets->Label(translate('This task work load is completed')));
        }
        
        return $widgets->Section(translate('Work load'), $layout);
    }
}
