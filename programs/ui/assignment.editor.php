<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Assignment;

class AssignmentEditor extends Editor
{
    
    /**
     * 
     * @var Assignment
     */
    private $assignment;
    
    public function __construct(Assignment $assignment)
    {
        parent::__construct();
        
        $this->assignment = $assignment;
        $this->setName('assignment');
        
        $this->setHiddenValue('assignment[TaskUID]', $assignment->TaskUID);
        
        $this->addFields();
        $this->addButtons();
        
        $values = $assignment->getValues();
        
        if (isset($_POST['assigment'])) {
            $values = $_POST['assignment'];
        }
        
        $this->setValues(array('assignment' => $values));
    }
    
    
    protected function addFields()
    {
        $widgets = \bab_Widgets();

        $this->addItem($this->resource());
        
        
        $this->addItem($widgets->FlowItems($this->start(), $this->finish())->setSpacing(2, 'em'));
        
        // will be calculated instead
        //$this->addItem($widgets->FlowItems($this->actualstart(), $this->actualfinish())->setSpacing(2, 'em'));
        
        $this->addItem($this->units());
        $this->addItem($this->work());
        $this->addItem($this->actualwork());
    }
    
    
    protected function addButtons()
    {
        $widgets = \bab_Widgets();
        
        $displayTask = controller()->task()->display($this->assignment->getFkPk('TaskUID'));

        $save = $widgets->SubmitButton()
            ->setAction(controller()->assignment()->save())
            ->setSuccessAction($displayTask)
            ->setFailedAction(controller()->assignment()->edit($this->assignment->getValues()))
            ->setLabel(translate('Save'));
        
        $cancel = $widgets->Link(
            translate('Cancel'),
            $displayTask
        )->addClass('cancel-button');
        
        $this->addItem($widgets->FlowItems($save, $cancel)->setVerticalAlign('middle')->setSpacing(1, 'em'));
    }
    
    

    protected function resource()
    {
        $widgets = \bab_Widgets();
        
        storage()->includeProjectSet();
        $project = $this->assignment->project();
        
        if (!isset($project)) {
            throw new \Exception('The assignment is not linked to a project');
        }
        
        $select = $widgets->Select();
        $resources = $project->selectResources();
        
        if (0 === $resources->count()) {
            throw new \bab_AccessException(translate('A resource in project need to be created first'));
        }
        
        foreach ($resources as $resource) {
            $select->addOption($resource->UID, $resource->Name);
        }
        
        return $widgets->LabelledWidget(
            translate('Ressource'),
            $select,
            'ResourceUID'
        );
    }
    
    
    protected function actualstart()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Actual start date'),
            $widgets->DateTimePicker(),
            'ActualStart'
        );
    
        return $item;
    }
    
    protected function actualfinish()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Actual finish date'),
            $widgets->DateTimePicker(),
            'ActualFinish'
        );
    
        return $item;
    }
    
    
    protected function start()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Schedule start date'),
            $widgets->DateTimePicker(),
            'Start'
        );
    
        return $item;
    }
    
    protected function finish()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Schedule finish date'),
            $widgets->DateTimePicker(),
            'Finish'
        );
    
        return $item;
    }
    
    
    protected function units()
    {
        $widgets = \bab_Widgets();
        
        return $widgets->LabelledWidget(
            translate('Number of units'),
            $widgets->LineEdit()->setSize(5)->setValue('1,00'),
            'Units',
            translate('If you modify Assignment Units directly, either work or duration changes, 
                depending on your task type setting. If your task type is fixed work and you modify 
                the duration, the assignment units is modified. If your task type field is 
                set to fixed duration and you modify work, assignment units will be modified')
        );
    }
    
    
    protected function work()
    {
        $widgets = \bab_Widgets();
        
        $duration = UiLoader::durationField();
        $duration->setName('Work');

        return $widgets->LabelledWidget(
            translate('Scheduled Work for the assignment'),
            $duration
        );
    }
    
    
    protected function actualwork()
    {
        $widgets = \bab_Widgets();
    
        $duration = UiLoader::durationField();
        $duration->setName('ActualWork');
    
        return $widgets->LabelledWidget(
            translate('Actual work (work done so far)'),
            $duration,
            null,
            translate("As you enter actual work information for tasks, 
            or percent of work complete, Project updates the assignment actual work value.")
        );
    }
}
