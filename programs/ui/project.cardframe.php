<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use Ovidentia\LibProject\Project;

class ProjectCardFrame extends UiFrame
{
    /**
     * @var Project
     */
    protected $project;

    
    
    public function __construct(Project $project)
    {
        $this->project = $project;
        
        parent::__construct(null, bab_Widgets()->VBoxLayout()->setVerticalSpacing(.8, 'em'));
        
        $this->loadItems();
        
        $this->addClass('project-cardframe');
    }
    
    /**
     * @return Project
     */
    protected function getRecord()
    {
        return $this->project;
    }
    
    
    protected function loadItems()
    {
        $widgets = bab_Widgets();
        // $this->addItem($widgets->Title($this->project->Title, 3));
        $stat = $this->project->statistics();
        
        $set = $this->project->getParentSet();
        
        $this->addItem($widgets->Title($this->project->Title, 4));
        $this->addItem($this->labeled(translate('Finish date'), $set->FinishDate));
        
        if ($work = (string) UiLoader::duration($stat->getRemainingWork())) {
            $this->addItem($this->text(translate('Remaining work'), $work));
        }
        
    }
}
