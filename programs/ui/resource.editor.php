<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Resource;

class ResourceEditor extends Editor
{
    /**
     * @var Resource
     */
    private $resource;
    
    /**
     * 
     * @var \Widget_Select
     */
    private $typeSelect;
    
    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
       
        parent::__construct();
    
        $this->setName('resource');
        $this->colon();
        $this->addClass(\Func_Icons::ICON_LEFT_16);
    
        $this->addFields();
        $this->addButtons();
    
        if ($resource->project) {
            $this->setHiddenValue('resource[project]', $resource->project);
        }
        
        if ($resource->UID) {
            $this->setHiddenValue('resource[UID]', $resource->UID);
        }
    
    
        $values = $resource->getValues();
        if (isset($_POST['resource'])) {
            $values = $_POST['resource'];
        }
    
        $this->setValues(array('resource' => $values));
    }
    
    
    protected function addFields()
    {
        $widgets = \bab_Widgets();
        
        $this->addItem(
            $widgets->FlowItems(
                $this->initials(),
                $this->type(),
                $materialLabel = $this->materialLabel(),
                $userPicker = $this->userPicker()
            )->setHorizontalSpacing(3, 'em')->setVerticalAlign('top')
        );
    
        $this->addItem($name = $this->name());
        $this->addItem($email = $this->emailAddress());

        
        $this->addItem(
            $widgets->HBoxItems(
                $this->group(),
                $this->workGroup()
            )->setHorizontalSpacing(3, 'em')->setVerticalAlign('top')
        );
        
        
        
        $this->addItem($this->notes());
        
        $this->typeSelect->setAssociatedDisplayable($materialLabel, array(Resource::MATERIAL));
        $this->typeSelect->setAssociatedDisplayable($email, array(Resource::WORK));
        $this->typeSelect->setAssociatedDisplayable($userPicker, array(Resource::WORK));
    }
    
    
    protected function addButtons()
    {
        $widgets = \bab_Widgets();
        
    
        $save = $widgets->SubmitButton()
        ->setAction(controller()->resource()->save())
        ->setSuccessAction(controller()->resource()->displayList($this->resource->getFkPk('project')))
        ->setFailedAction(controller()->resource()->edit($this->resource->getValues()))
        ->setLabel(translate('Save'));
    
        $cancel = $widgets->Link(
            translate('Cancel'),
            controller()->resource()->displayList($this->resource->getFkPk('project'))
        )->addClass('cancel-button');
    
        $this->addItem($widgets->FlowItems($save, $cancel)->setVerticalAlign('middle')->setSpacing(1, 'em'));
    }
    
    
    protected function userPicker()
    {
        $widgets = \bab_Widgets();

        $item = $widgets->LabelledWidget(
            translate('Link this resource to an ovidentia user'),
            $widgets->UserPicker(),
            'user'
        );
        
        return $item;
    }
    
    
    protected function name()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Name'),
            $widgets->LineEdit()->addClass('widget-fullwidth')->setMandatory(true, translate('The name is mandatory')),
            'Name'
        );

        return $item;
    }
    
    
    
    protected function type()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Type'),
            $this->typeSelect = $widgets->Select()->setOptions($this->resource->getParentSet()->getTypes()),
            'Type'
        );
    }
    
    
    
    protected function initials()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Initials'),
            $widgets->LineEdit()->setSize(6)->setMaxSize(10),
            'Initials'
        );

        return $item;
    }
    
    
    
    protected function materialLabel()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Material label'),
            $widgets->LineEdit()->setSize(6)->setMaxSize(10),
            'MaterialLabel',
            translate('The unit of measure for the material resource')
        );
    
        return $item;
    }
    
    
    
    protected function group()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Group'),
            $widgets->LineEdit()->addClass('widget-fullwidth'),
            'Group'
        );
        
        $item->setSizePolicy(\Widget_SizePolicy::MAXIMUM);

        return $item;
    }
    
    
    protected function workGroup()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Work group type'),
            $widgets->Select()->setOptions($this->resource->getParentSet()->getWorkGroups()),
            'WorkGroup'
        )->addClass('widget-nowrap');
    }
    
    
    protected function emailAddress()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Email address'),
            $widgets->LineEdit()->addClass('widget-fullwidth'),
            'EmailAddress'
        );

        return $item;
    }
    
    protected function notes()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Notes'),
            $widgets->TextEdit()->addClass('widget-fullwidth'),
            'Notes'
        );

        return $item;
    }
}
