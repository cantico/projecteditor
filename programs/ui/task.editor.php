<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Task;

class TaskEditor extends Editor
{
    /**
     * @var Task
     */
    private $task;
    
    public function __construct(Task $task)
    {
        $this->task = $task;
       
        parent::__construct();
    
        $this->setName('task');
        $this->colon();
    
        $this->addFields();
        $this->addButtons();
    
        if ($task->project) {
            $this->setHiddenValue('task[project]', $task->getFkPk('project'));
        }
        
        if ($task->UID) {
            $this->setHiddenValue('task[UID]', $task->UID);
        }
    
    
        $values = $task->getValues();
        if (isset($_POST['task'])) {
            $values = $_POST['task'];
        }
    
        $this->setValues(array('task' => $values));
    }
    
    
    protected function addFields()
    {
        $widgets = \bab_Widgets();
    
        $this->addItem($this->name());
        
        $this->addItem($this->type());
        
        
        $this->addItem(
            $widgets->FlowItems(
                $this->effortDriven(),
                $this->recurring(),
                $this->estimated(),
                $this->milestone(),
                $this->summary(),
                $this->critical(),
                $this->ignoreResourceCalendar(),
                $this->active()
            )->setHorizontalSpacing(3, 'em')
        );
        
        $this->addItem($this->actual());
        $this->addItem($this->scheduled());
        
        $this->addItem($this->deadline());
        $this->addItem($this->constraint());
        
        $this->addItem($this->notes());
    }
    
    
    protected function addButtons()
    {
        $widgets = \bab_Widgets();
        
        
        if ($this->task->UID) {
            $target = controller()->task()->display($this->task->UID);
        } else {
            $target = controller()->task()->displayList($this->task->getFkPk('project'));
        }
        
    
        $save = $widgets->SubmitButton()
        ->setAction(controller()->task()->save())
        ->setSuccessAction($target)
        ->setFailedAction(controller()->task()->edit($this->task->getValues()))
        ->setLabel(translate('Save'));
    
        $cancel = $widgets->Link(
            translate('Cancel'),
            $target
        )->addClass('cancel-button');
    
        $this->addItem($widgets->FlowItems($save, $cancel)->setVerticalAlign('middle')->setSpacing(1, 'em'));
    }
    
    
    protected function name()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Name'),
            $widgets->LineEdit()->addClass('widget-fullwidth')->setMandatory(true, translate('The name is mandatory')),
            'Name'
        );

        return $item;
    }
    
    
    
    protected function type()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Type'),
            $widgets->Select()->setOptions($this->task->getParentSet()->getTypes()),
            'Type'
        );
    }
    
    protected function scheduled()
    {
        return $this->period('Start', 'Finish', translate('Scheduled task dates'));
    }
    
    
    protected function actual()
    {
        return $this->period('ActualStart', 'ActualFinish', translate('Actual task dates'));
    }
    
    
    /**
     * A datetime period
     */
    protected function period($startname, $finishname, $title)
    {
        $widgets = \bab_Widgets();
        
        $period = $widgets->FlowItems(
            $this->datetime($startname, translate('Start')),
            $this->datetime($finishname, translate('Finish'))
        )->setHorizontalSpacing(2, 'em');
        
        
        return $widgets->VBoxItems(
            $widgets->Label($title)->addClass('widget-strong'),
            $period
        )->setVerticalSpacing(.4, 'em');
    }
    
    
    protected function datetime($name, $text)
    {
        $widgets = \bab_Widgets();
    
        $label = $widgets->Label($text);
        $input = $widgets->DateTimePicker()->setName($name);
        $label->setAssociatedWidget($input);
        
        
        return $widgets->FlowItems($label, $input)->setHorizontalSpacing(.5, 'em');
    }
    
    
    
    
    protected function effortDriven()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Effort driven'),
            $widgets->CheckBox(),
            'EffortDriven'
        );
    
    }
    
    
    
    protected function recurring()
    {
        $widgets = \bab_Widgets();

        return $widgets->LabelledWidget(
            translate('Recurring'),
            $widgets->CheckBox(),
            'Recurring'
        );
    
    }
    
    
    protected function estimated()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Estimated'),
            $widgets->CheckBox(),
            'Estimated'
        );
    }
    
    
    protected function milestone()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Milestone'),
            $widgets->CheckBox(),
            'Milestone'
        );
    }
    
    
    protected function summary()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Summary'),
            $widgets->CheckBox(),
            'Summary'
        );
    }
    
    protected function critical()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Critical'),
            $widgets->CheckBox(),
            'Critical'
        );
    }
    
    protected function ignoreResourceCalendar()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Ignore resource calendar'),
            $widgets->CheckBox(),
            'IgnoreResourceCalendar'
        );
    }
    
    
    protected function active()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Active'),
            $widgets->CheckBox(),
            'Active'
        );
    }
    
    
    
    
    protected function constraintType()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Constraint type'),
            $widgets->Select()->setOptions($this->task->getParentSet()->getConstraintTypes()),
            'ConstraintType'
        );
    }
    
    
    protected function constraintDate()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Constraint date'),
            $widgets->DateTimePicker(),
            'ConstraintDate'
        );
    
        return $item;
    }
    
    
    protected function constraint()
    {
        $widgets = \bab_Widgets();
        $date = $this->constraintDate();
        
        $type = $this->constraintType();
        $type->getInputWidget()->setAssociatedDisplayable(
            $date,
            array(
                Task::START_ON,
                Task::FINISH_ON,
                Task::START_NO_EARLIER_THAN,
                Task::START_NO_LATER_THAN,
                Task::FINISH_NO_EARLIER_THAN,
                Task::FINISH_NO_LATER_THAN
            )
        );
        
        return $widgets->FlowItems($type, $date)->setHorizontalSpacing(1, 'em');
    }
    
    
    protected function deadline()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Deadline'),
            $widgets->DateTimePicker(),
            'Deadline'
        );
    
        return $item;
    }
    
    protected function notes()
    {
        $widgets = \bab_Widgets();
    
        $item = $widgets->LabelledWidget(
            translate('Notes'),
            $widgets->TextEdit()->addClass('widget-fullwidth'),
            'Notes'
        );
    
        return $item;
    }
}
