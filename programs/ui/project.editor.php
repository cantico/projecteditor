<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

class ProjectEditor extends Editor
{
    /**
     * @var \Ovidentia\LibProject\Project
     */
    private $project;
    
    public function __construct(\Ovidentia\LibProject\Project $project)
    {
        $this->project = $project;
        parent::__construct();
        
        $this->setName('project');
        
        $this->addFields();
        $this->addButtons();
        
        if ($project->uuid) {
            $this->setHiddenValue('project[uuid]', $project->uuid);
        }
        
        
        $values = $project->getValues();
        if (isset($_POST['project'])) {
            $values = $_POST['project'];
        }
        
        $this->setValues(array('project' => $values));
    }
    
    
    protected function addFields()
    {
        $widgets = \bab_Widgets();
        
        $this->addItem(
            $widgets->HBoxItems(
                $this->name(),
                $this->title()
            )->setHorizontalSpacing(2, 'em')
        );
        
        $this->addItem($this->subject());
        
        $this->addItem(
            $widgets->HBoxItems(
                $this->category(),
                $this->manager()
            )->setHorizontalSpacing(2, 'em')
        );
        
        
        $this->addItem(
            $widgets->HBoxItems(
                $this->startDate(),
                $this->finishDate(),
                $this->scheduleFromStart()
            )->setHorizontalSpacing(2, 'em')
        );
        
        $this->addItem($this->company());
        
        $this->addItem(
            $widgets->HBoxItems(
                $this->author(),
                $this->defaultStartTime(),
                $this->defaultFinishTime()
            )->setHorizontalSpacing(2, 'em')
        );
    }
    
    
    protected function addButtons()
    {
        $widgets = \bab_Widgets();
        
        $save = $widgets->SubmitButton()
            ->setAction(controller()->project()->save())
            ->setSuccessAction(controller()->project()->displayList())
            ->setFailedAction(controller()->project()->edit())
            ->setLabel(translate('Save'));
        
        $cancel = $widgets->Link(
            translate('Cancel'),
            controller()->project()->displayList()
        )->addClass('cancel-button');
        
        $this->addItem($widgets->FlowItems($save, $cancel)->setVerticalAlign('middle')->setSpacing(1, 'em'));
    }
    
    
    protected function title()
    {
        $widgets = \bab_Widgets();
    
        $title = $widgets->LabelledWidget(
            translate('Title'),
            $widgets->LineEdit()->addClass('widget-fullwidth')->setMandatory(true, translate('The title is mandatory')),
            'Title'
        );
        
        $title->setSizePolicy(\Widget_SizePolicy::MAXIMUM);
        
        return $title;
    }
    
    protected function name()
    {
        $widgets = \bab_Widgets();
        
        return $widgets->LabelledWidget(
            translate('Name'),
            $widgets->LineEdit()->setSize(15),
            'Name'
        );
    }
    
    protected function author()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Author'),
            $widgets->LineEdit()->setSize(30),
            'Author'
        );
    }
    
    
    protected function category()
    {
        $widgets = \bab_Widgets();
    
        $category = $widgets->LabelledWidget(
            translate('Category'),
            $widgets->LineEdit()->addClass('widget-fullwidth'),
            'Category'
        );
        
        $category->setSizePolicy(\Widget_SizePolicy::MAXIMUM);
        
        return $category;
    }
    
    
    protected function subject()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Subject'),
            $widgets->LineEdit()->addClass('widget-fullwidth'),
            'Subject'
        );
    }
    
    
    protected function company()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Company'),
            $widgets->LineEdit()->addClass('widget-fullwidth'),
            'Company'
        );
    }
    
    
    protected function manager()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Manager'),
            $widgets->LineEdit()->setSize(30),
            'Manager'
        );
    }
    
    protected function startDate()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Start date'),
            $widgets->DateTimePicker(),
            'StartDate'
        );
    }
    
    
    protected function finishDate()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Finish date'),
            $widgets->DateTimePicker(),
            'FinishDate'
        );
    }
    
    
    protected function scheduleFromStart()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Schedule tasks from'),
            $widgets->RadioSet()->addOption(1, translate('Start date'))->addOption(0, translate('Finish date')),
            'ScheduleFromStart'
        );
    }
    
    
    protected function defaultStartTime()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Default start time'),
            $widgets->TimePicker(),
            'DefaultStartTime'
        );
    }
    
    
    protected function defaultFinishTime()
    {
        $widgets = \bab_Widgets();
    
        return $widgets->LabelledWidget(
            translate('Default finish time'),
            $widgets->TimePicker(),
            'DefaultFinishTime'
        );
    }
}
