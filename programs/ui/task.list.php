<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\TaskSet;
use \Ovidentia\LibProject\Task;

class TaskList extends \widget_TableModelView
{
    public function addDefaultColumns(TaskSet $set)
    {
        $this->addColumn(\widget_TableModelViewColumn($set->Name, translate('Name')));
        $this->addColumn(\widget_TableModelViewColumn($set->Type, translate('Type')));
        $this->addColumn(\widget_TableModelViewColumn($set->Active, translate('Active')));
        $this->addColumn(\widget_TableModelViewColumn($set->ConstraintType, translate('Constraint type')));
        $this->addColumn(\widget_TableModelViewColumn($set->ConstraintDate, translate('Constraint date')));
        $this->addColumn(\widget_TableModelViewColumn($set->Critical, translate('Critical'))->setVisible(false));
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Deadline, translate('Deadline'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->EffortDriven, translate('Effort driven'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Estimated, translate('Estimated'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Duration, translate('Duration'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->ActualDuration, translate('Actual duration'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->OvertimeWork, translate('Overtime work'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->ActualOvertimeWork, translate('Actual overtime work'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->RemainingDuration, translate('Remaining duration'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->RemainingWork, translate('Remaining work'))
            ->setVisible(false)
        );
        
        $this->addColumn(\widget_TableModelViewColumn($set->RegularWork, translate('Regular work'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Work, translate('Work'))->setVisible(false));
        
        
        $this->addColumn(\widget_TableModelViewColumn($set->Start, translate('Start'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->ActualStart, translate('Actual start')));
        $this->addColumn(\widget_TableModelViewColumn($set->Finish, translate('Finish'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->ActualFinish, translate('Actual finish')));
        
        $this->addColumn(\widget_TableModelViewColumn($set->EarlyStart, translate('Early start'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->LateStart, translate('Late start'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->EarlyFinish, translate('Early finish'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->LateFinish, translate('Late finish'))->setVisible(false));
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->IgnoreResourceCalendar, translate('Ignore resource calendar'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Hyperlink, translate('Link'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Milestone, translate('Milestone'))
            ->setVisible(true)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->PercentComplete, translate('Completion'))
            ->setVisible(true)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->PercentWorkComplete, translate('Work completion'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Priority, translate('Priority'))
            ->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Recurring, translate('Recurring task'))
            ->setVisible(false)
        );
        
        /*
        $this->addColumn(
            \widget_TableModelViewColumn($set->Stop, translate('Stop'))->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->Resume, translate('Resume'))->setVisible(false)
        );
        
        $this->addColumn(
            \widget_TableModelViewColumn($set->ResumeValid, translate('Resume valid'))->setVisible(false)
        );
        */
        
        $this->addColumn(\widget_TableModelViewColumn($set->Summary, translate('Summary'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->WBS, translate('WBS'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->WBSLevel, translate('WBS Level'))->setVisible(false));
        
        
        return $this;
    }
    
    /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param    string            $name        table field name
     * @param     \ORM_Field        $field        ORM field
     *
     * @return Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        
        switch($name) {
            case 'Name':
            case 'Type':
                return parent::handleFilterInputWidget($name, $field);
        }
        
        
    }
    
    
    
    /**
     *
     * @param Task      $record
     * @param string    $fieldPath
     * @return \Widget_Displayable_Interface    The item that will be placed in the cell
     */
    protected function computeCellContent(Task $record, $fieldPath)
    {
        $widgets = \bab_Widgets();
        
        switch($fieldPath) {
            case 'Name':
                return $widgets->Link($record->Name, controller()->task()->display($record->UID));
            case 'Hyperlink':
                if ($record->HyperlinkAddress) {
                    return $widgets->Link($record->Hyperlink, $record->HyperlinkAddress);
                }
                return $widgets->Link($record->Hyperlink, $record->Hyperlink);
            case 'PercentComplete':
            case 'PercentWorkComplete':
                $gauge = $widgets->Gauge();
                // $gauge->setCanvasOptions($gauge->Options()->width(8, 'em'));
                return $gauge->setProgress($record->$fieldPath);
            case 'Duration':
            case 'ActualDuration':
            case 'OvertimeWork':
            case 'ActualOvertimeWork':
            case 'RemainingDuration':
            case 'RemainingWork':
            case 'RegularWork':
            case 'Work':
                if (!empty($record->$fieldPath)) {
                    $set = $record->getParentSet();
                    $interval = $set->getInterval($record->$fieldPath);
                    $duration = UiLoader::duration($interval);
                    return $duration->getLabel();
                }
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}
