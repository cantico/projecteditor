<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use Ovidentia\LibProject\Record;

class UiFrame extends \Widget_Frame
{
    /**
     * Label record field
     *
     * @param string $title
     * @param \ORM_Field $field
     * @return \Widget_Displayable_Interface
     */
    protected function labeled($title, \ORM_Field $field)
    {
        $widgets = bab_Widgets();
        $fieldName = $field->getName();
    
        $value = $field->output($this->getRecord()->$fieldName);
    
        if ('' === $value) {
            return null;
        }
    
        return $this->text($title, $value);
    }
    
    /**
     * @return Record
     */
    protected function getRecord()
    {
        return null;
    }
    
    
    /**
     * text value with title
     *
     * @param string $title
     * @param string | \Widget_Label $value
     * @return \Widget_Displayable_Interface
     */
    protected function text($title, $value)
    {
        $widgets = bab_Widgets();
    
        if (!($value instanceof \Widget_Label)) {
    
            if ('' === $value) {
                return null;
            }
    
            $value = $widgets->Label($value);
        } elseif ('' === $value->getText()) {
            return null;
        }
    
        return $widgets->FlowItems(
            $widgets->Label($title)->colon()->addClass('widget-strong'),
            $value
        )->setSpacing(0.5, 'em');
    }
}
