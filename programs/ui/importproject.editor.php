<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Project;

class ImportProjectEditor extends Editor
{
    public function __construct()
    {
        parent::__construct();
        
        $this->setName('importproject');
        
        $this->addFields();
        $this->addButtons();
        
        $values = array();
        if (isset($_POST['importproject'])) {
            $values = $_POST['importproject'];
        }
        
        $this->setValues(array('importproject' => $values));
    }
    
    
    protected function addFields()
    {
        $widgets = \bab_Widgets();

        $this->addItem(
            $widgets->HBoxItems(
                $this->file(),
                $widgets->label(
                    translate(
                        'MSPDI files can be exported from Microsoft project, 
                        Office 360 Projects, openproj, Ganttproject. 
                        A new project will be created with the content of the file.'
                    )
                )
            )->setVerticalAlign('middle')->setHorizontalSpacing(4, 'em')
        );
    }
    
    
    protected function addButtons()
    {
        $widgets = \bab_Widgets();
        
        $save = $widgets->SubmitButton()
            ->setAction(controller()->project()->saveImport())
            ->setSuccessAction(controller()->project()->displayList())
            ->setFailedAction(controller()->project()->import())
            ->setLabel(translate('Import as a new project'));
        
        $cancel = $widgets->Link(
            translate('Cancel'),
            controller()->project()->displayList()
        )->addClass('cancel-button');
        
        $this->addItem($widgets->FlowItems($save, $cancel)->setVerticalAlign('middle')->setSpacing(1, 'em'));
    }
    
    

    protected function file()
    {
        $widgets = \bab_Widgets();
        
        return $widgets->LabelledWidget(
            translate('Import a project file in MSPDI format (.xml)'),
            $widgets->FilePicker()->oneFileMode(),
            'file'
        );
    }
}
