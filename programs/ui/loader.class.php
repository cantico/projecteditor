<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
*/
namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Project;
use \Ovidentia\LibProject\Task;
use \Ovidentia\LibProject\Resource;
use \Ovidentia\LibProject\Assignment;

class UiLoader
{
    private static function widget($className)
    {
        $widgets = bab_Widgets();
        $widgets->includePhpClass($className);
    }
    
    /**
     * Include the base page
     */
    private static function includePage()
    {
        self::widget('widget_ContextPage');
        require_once dirname(__FILE__).'/page.php';
    }
    
    
    private static function includeUiFrame()
    {
        self::widget('widget_Frame');
        require_once dirname(__FILE__).'/uiframe.php';
    }
    
    
    /**
     * @return Page
     */
    public static function page()
    {
        self::includePage();
        
        return new Page();
    }
    
    
    /**
     * Project list page
     */
    public static function projectListPage()
    {
        self::includePage();
        require_once dirname(__FILE__).'/projectlist.page.php';
        
        return new ProjectListPage();
    }
    
    
    public static function projectPage(Project $project)
    {
        self::includePage();
        require_once dirname(__FILE__).'/project.page.php';
        
        $page = new ProjectPage();
        $page->setProject($project);
        return $page;
    }
    
    
    /**
     * Tasks list page
     */
    public static function taskListPage(Project $project)
    {
        self::includePage();
        require_once dirname(__FILE__).'/project.page.php';
        require_once dirname(__FILE__).'/tasklist.page.php';
    
        return new TaskListPage($project);
    }
    
    
    /**
     * Tasks list page
     */
    public static function resourceListPage(Project $project)
    {
        self::includePage();
        require_once dirname(__FILE__).'/project.page.php';
        require_once dirname(__FILE__).'/resourcelist.page.php';
    
        return new ResourceListPage($project);
    }
    
    
    
    /**
     * @return ProjectList
     */
    public static function projectList()
    {
        self::widget('widget_TableModelView');
        require_once dirname(__FILE__).'/project.list.php';
        
        return new ProjectList();
    }
    
    /**
     * Include the base editor
     */
    private static function includeEditor()
    {
        self::widget('widget_Form');
        require_once dirname(__FILE__).'/editor.php';
    }
    
    /**
     * Include the base editor
     */
    private static function includeTableModelView()
    {
        self::widget('widget_TableModelView');
        require_once dirname(__FILE__).'/tablemodelview.php';
    }
    
    
    /**
     * @return ProjectEditor
     */
    public static function projectEditor(Project $project)
    {
        self::includeEditor();
        require_once dirname(__FILE__).'/project.editor.php';
        
        return new ProjectEditor($project);
    }
    
    /**
     * @return ProjectCardFrame
     */
    public static function projectCardFrame(Project $project)
    {
        self::includeUiFrame();
        require_once dirname(__FILE__).'/project.cardframe.php';
    
        return new ProjectCardFrame($project);
    }
    
    /**
     * @return ProjectFullFrame
     */
    public static function projectFullFrame(Project $project)
    {
        self::includeUiFrame();
        require_once dirname(__FILE__).'/project.fullframe.php';
        
        return new ProjectFullFrame($project);
    }
    
    
    /**
     * @return ResourceList
     */
    public static function resourceList()
    {
        self::widget('widget_TableModelView');
        require_once dirname(__FILE__).'/resource.list.php';
        
        return new ResourceList();
    }
    
    public static function resourceEditor(Resource $resource)
    {
        self::includeEditor();
        require_once dirname(__FILE__).'/resource.editor.php';
        
        return new ResourceEditor($resource);
    }
    
    
    
    public static function taskPage(Task $task)
    {
        self::includePage();
        require_once dirname(__FILE__).'/project.page.php';
        require_once dirname(__FILE__).'/task.page.php';
    
        $page = new TaskPage();
        $page->setTask($task);
        return $page;
    }
    
    
    
    public static function taskList()
    {
        self::widget('widget_TableModelView');
        require_once dirname(__FILE__).'/task.list.php';
        
        return new TaskList();
    }
    
    
    /**
     * @return TaskFullFrame
     */
    public static function taskFullFrame(Task $task)
    {
        self::includeUiFrame();
        require_once dirname(__FILE__).'/task.fullframe.php';
    
        return new TaskFullFrame($task);
    }
    
    
    
    public static function taskEditor(Task $task)
    {
        self::includeEditor();
        require_once dirname(__FILE__).'/task.editor.php';
        
        return new TaskEditor($task);
    }

    
    /**
     * duration object used to convert an interval to a string or label
     * @return Duration
     */
    public static function duration(\DateInterval $interval = null)
    {
        require_once dirname(__FILE__).'/duration.class.php';
        return new Duration($interval);
    }
    
    /**
     * @return DurationField
     */
    public static function durationField()
    {
        require_once dirname(__FILE__).'/duration.field.php';
        return new DurationField();
    }
    
    
    /**
     * Import project editor
     * @return ImportProjectEditor
     */
    public static function importProjectEditor()
    {
        self::includeEditor();
        require_once dirname(__FILE__).'/importproject.editor.php';
        
        return new ImportProjectEditor();
    }
    
    /**
     * Assignment list of a task
     * @param Task $task
     * @return AssignmentList
     */
    public static function assignmentList(Task $task)
    {
        self::includeTableModelView();
        require_once dirname(__FILE__).'/assignment.list.php';
        
        $list = new AssignmentList();
        $res = $task->selectAssignments();
        $list->setDataSource($task->selectAssignments());
        $list->addDefaultColumns($res->getSet());
        
        return $list;
    }
    
    
    /**
     * @param Assignment $assignment
     * @return AssignmentEditor
     */
    public static function assignmentEditor(Assignment $assignment)
    {
        self::includeEditor();
        require_once dirname(__FILE__).'/assignment.editor.php';
    
        return new AssignmentEditor($assignment);
    }
}
