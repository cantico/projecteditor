<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\Project;

class ProjectPage extends Page
{
    
    public function setProject(Project $project)
    {
        $this->addProjectContextMenu($project);
        $this->setTitle($project->Title);
    }
    
    
    /**
     * Add project visualisation context menu
     */
    public function addProjectContextMenu(Project $project)
    {
        $this->addItemMenu(
            'project.display',
            translate('Project home'),
            controller()->project()->display($project->uuid)->url()
        );
        
        $this->addItemMenu(
            'task.displayList',
            translate('Tasks'),
            controller()->task()->displayList($project->uuid)->url()
        );
        
        $this->addItemMenu(
            'resource.displayList',
            translate('Resources'),
            controller()->resource()->displayList($project->uuid)->url()
        );
        
        $action = \Func_Widgets::Action()->fromRequest();
        $this->setCurrentItemMenu($action->getMethod());
    }
    
    
    
    public function addProjectContextButtons(Project $project)
    {
        $widgets = \bab_Widgets();
        
        // right collumn content
        
        $this->addContextLink(
            $widgets->Link(
                $widgets->Icon(
                    translate('Edit project'),
                    \Func_Icons::ACTIONS_DOCUMENT_EDIT
                ),
                controller()->project()->edit($project->uuid)
            )
        );
        
        $this->addContextLink(
            $widgets->Link(
                $widgets->Icon(
                    translate('Delete project'),
                    \Func_Icons::ACTIONS_EDIT_DELETE
                ),
                controller()->project()->delete($project->uuid)
            )->setConfirmationMessage(translate('Do you really want to delete the project?'))
        );
        
        
    }
}
