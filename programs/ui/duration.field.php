<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;


/**
 * Duration field
 * years are converted to month
 * minutes, seconds are converted to hours
 * month, days, hours are editables
 */
class DurationField extends \Widget_ContainerWidget implements \Widget_Displayable_Interface
{
    /**
     * @var \DateInterval
     */
    private $interval;
    
    /**
     * @var \Widget_LineEdit
     */
    private $month;
    
    /**
     * @var \Widget_LineEdit
     */
    private $day;
    
    /**
     * @var \Widget_LineEdit
     */
    private $hour;
    
    
    
    
    /**
     * @param string $id            The item unique id.
     * @return Widget_DatePicker
     */
    public function __construct($id = null)
    {
        $widgets = \bab_Widgets();
        
        $widgets->includePhpClass('Widget_FlowLayout');
        $widgets->includePhpClass('Widget_LineEdit');
        
        $layout = $this->flow();
    
        parent::__construct($id, $layout);
    
        $this->month     = new \Widget_LineEdit($this->getId().'_month');
        $this->day       = new \Widget_LineEdit($this->getId().'_day');
        $this->hour      = new \Widget_LineEdit($this->getId().'_hour');
        
        $this->month->setSize(5);
        $this->day->setSize(5);
        $this->hour->setSize(5);
    
        $this->month->setName('month');
        $this->day->setName('day');
        $this->hour->setName('hour');
    
        $unit = $widgets->Select()->setName('unit');
        $unit->addOption('month', translate('Months'));
        $unit->addOption('day', translate('Days'));
        $unit->addOption('hour', translate('Hours'));
        $unit->setValue('day');
    
        $layout
        ->setHorizontalSpacing(1, 'em')
        ->setHorizontalSpacing(.3, 'em')
        ->setVerticalAlign('middle')
        ->addItem(
            $this->flow()
            ->addItem($this->month)
            ->addItem($this->day)
            ->addItem($this->hour)
        )
        ->addItem($unit);
        
        $unit->setAssociatedDisplayable($this->month, array('month'));
        $unit->setAssociatedDisplayable($this->day, array('day'));
        $unit->setAssociatedDisplayable($this->hour, array('hour'));
    }
    
    
    private function flow()
    {
        $widgets = \bab_Widgets();
        return $widgets->FlowLayout();
    }
    
    
    
    /**
     * Set value by date interval
     * @param \DateInterval $interval
     * 
     * @return DurationField
     */
    public function setDateInterval(\DateInterval $interval)
    {
        $months = $interval->m;
        if ($interval->y) {
            $months += 12*$interval->y;
        }
        
        $this->setMonths($months);
        
        $this->setDays($interval->d);
        
        $hours = $this->h;
        $minutes = $interval->i;
        if ($interval->s) {
            $minutes += round($interval->s/60);
        }
        
        if ($minutes > 0) {
            $hours += round($minutes/60);
        }
        
        $this->setHours($hours);
        
        return $this;
    }
    
    /**
     * Set months
     * @param   int $months
     * @return DurationField
     */
    public function setMonths($months)
    {
        $this->month->setValue((int) $months);
        return $this;
    }
    
    /**
     * Set days
     * @param   int $days
     * @return DurationField
     */
    public function setDays($days)
    {
        $this->day->setValue((int) $days);
        return $this;
    }
    
    /**
     * Set hours
     * @param   int $hours
     * @return DurationField
     */
    public function setHours($hours)
    {
        $this->hour->setValue((int) $hours);
        return $this;
    }
    
    /**
     * Set value, if not a duration, must be an array with month day hour as keys
     * @param array | string $value
     */
    public function setValue($value)
    {
        if (preg_match('/P[TYMDHMS0-9]+/', $value)) {
            return $this->setDateInterval(new \DateInterval($value));
        }
        
        if (!is_array($value)) {
            throw \UnexpectedValueException('must be an array or duration');
        }
        
        if (isset($value['month'])) {
            $this->setMonths($value['month']);
        }
        
        if (isset($value['day'])) {
            $this->setMonths($value['day']);
        }
        
        if (isset($value['hour'])) {
            $this->setMonths($value['hour']);
        }
        
        return $this;
    }
    
    
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'projecteditor-durationfield';
        return $classes;
    }
    
    
    public function display(\Widget_Canvas $canvas)
    {
        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array($this->getLayout()->display($canvas))
        );
    }
}
