<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

class ProjectFullFrame extends UiFrame
{
    /**
     * @var \Ovidentia\LibProject\Project
     */
    protected $project;
    
    
    /**
     * @var int
     */
    private $statTableRow = 0;
    
    
    public function __construct(\Ovidentia\LibProject\Project $project)
    {
        $this->project = $project;
        
        parent::__construct(null, bab_Widgets()->VBoxLayout()->setVerticalSpacing(.8, 'em'));
        
        $this->loadItems();
        
        $this->addClass('project-fullframe');
    }
    
    
    
    /**
     * @return Project
     */
    protected function getRecord()
    {
        return $this->project;
    }
    
    
    protected function loadItems()
    {
        $set = $this->project->getParentSet();
        
        $this->addItem($this->labeled(translate('Name'), $set->Name));

        
        $this->addItem($this->projectMetadata());
        $this->addItem($this->scheduling());
        $this->addItem($this->tasksStatistics());
    }
    
    
    protected function projectMetadata()
    {
        $widgets = bab_Widgets();
        $set = $this->project->getParentSet();
        
        $project = $widgets->Section(translate('Project metadata'));
        
        $project->addItem($this->labeled(translate('Manager'), $set->Manager));
        
        $project->addItem($this->labeled(translate('Subject'), $set->Subject));
        $project->addItem($this->labeled(translate('Category'), $set->Category));
        $project->addItem($this->labeled(translate('Company'), $set->Company));
        $project->addItem($this->labeled(translate('Creation date'), $set->CreationDate));
        $project->addItem($this->labeled(translate('Last saved date'), $set->LastSaved));
        $project->addItem($this->labeled(translate('Revision'), $set->Revision));
        
        return $project;
    }
    
    
    protected function scheduling()
    {
        $widgets = bab_Widgets();
        $set = $this->project->getParentSet();
        
        $scheduling = $widgets->Section(translate('Tasks scheduling'));
        
        $scheduling->addItem($this->labeled(translate('Start date'), $set->StartDate));
        $scheduling->addItem($this->labeled(translate('Finish date'), $set->FinishDate));
        
        if ($this->project->ScheduleFromStart) {
            $scheduling->addItem($widgets->Label(translate('Tasks are scheduled from the start date')));
        } else {
            $scheduling->addItem($widgets->Label(translate('Tasks are scheduled from the end date')));
        }
        
        return $scheduling;
    }
    
    
    /**
     * @param \DateInterval | string $value
     * @return \Widget_Label
     */
    private function tableValue($value)
    {
        $widgets = bab_Widgets();
        
        if ($value instanceof \DateInterval) {
            return UiLoader::duration($value)->getLabel();
        }
        
        return $widgets->Label($value);
    }
    
    /**
     * Add a table row
     * 
     * @param \Widget_BabTableView $table
     * @param string $title
     * @param \DateInterval | string $planned
     * @param \DateInterval | string $actual
     */
    private function tableRow(\Widget_BabTableView $table, $title, $planned, $actual)
    {
        $widgets = bab_Widgets();
        
        $this->statTableRow++;
        $table->addItem($widgets->label($title), $this->statTableRow, 0);
        $table->addItem($this->tableValue($planned), $this->statTableRow, 1);
        $table->addItem($this->tableValue($actual), $this->statTableRow, 2);
    }
    
    
    protected function tasksStatistics()
    {
        $widgets = bab_Widgets();
        $stat = $this->project->statistics();
        
        $set = storage()->taskSet();
        
        $section = $widgets->Section(
            translate('Tasks statistics'),
            $widgets->HBoxLayout()->setHorizontalSpacing(4, 'em')
        );
        
        $table = $widgets->BabTableView();
        $table->addHeadRow($this->statTableRow);
        $table->addItem(null, $this->statTableRow, 0);
        $table->addItem($widgets->Label(translate('Planned')), $this->statTableRow, 1);
        $table->addItem($widgets->Label(translate('Actual')), $this->statTableRow, 2);
        
        $this->tableRow(
            $table,
            translate('Duration'),
            $stat->getDuration(),
            $stat->getActualDuration()
        );
        
        $this->tableRow(
            $table,
            translate('Work'),
            $stat->getWork(),
            $stat->getActualWork()
        );

        $this->tableRow(
            $table,
            translate('Overtime work'),
            $stat->getOvertimeWork(),
            $stat->getActualOvertimeWork()
        );

        $this->tableRow(
            $table,
            translate('Start'),
            $set->Start->output($stat->getStart()),
            $set->ActualStart->output($stat->getActualStart())
        );
        
        $this->tableRow(
            $table,
            translate('Finish'),
            $set->Finish->output($stat->getFinish()),
            $set->ActualFinish->output($stat->getActualFinish())
        );

        
        
        $column = $widgets->VBoxLayout();
        
        $column->addItem(
            $this->text(
                translate('Regular work (non overtime work)'),
                UiLoader::duration($stat->getRegularWork())->getLabel()
            )
        );
        
        $column->addItem(
            $this->text(
                translate('Remaining work'),
                UiLoader::duration($stat->getRemainingWork())->getLabel()
            )
        );
        
        $column->addItem(
            $this->text(
                translate('Remaining duration'),
                UiLoader::duration($stat->getRemainingDuration())
            )
        );
        
        $column->addItem(
            $this->text(
                translate('First early start'),
                $set->EarlyStart->output($stat->getEarlyStart())
            )
        );
        
        $column->addItem(
            $this->text(
                translate('Last late finish'),
                $set->LateFinish->output($stat->getLateFinish())
            )
        );
        
        $column->addItem(
            $this->text(
                translate('Average complete'),
                $stat->getPercentComplete().'%'
            )
        );
        
        $column->addItem(
            $this->text(
                translate('Average work complete'),
                $stat->getPercentWorkComplete().'%'
            )
        );
        
        $section->addItem($column);
        $section->addItem($table);
        
        return $section;
    }
}
