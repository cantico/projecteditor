<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\AssignmentSet;
use \Ovidentia\LibProject\Assignment;

class AssignmentList extends TableModelView
{
    public function addDefaultColumns(AssignmentSet $set)
    {
        $this->addColumn(\widget_TableModelViewColumn('_edit', translate('Edit')));
        $this->addColumn(\widget_TableModelViewColumn($set->ActualStart, translate('Actual start'))->setVisible(false));
        $this->addColumn(
            \widget_TableModelViewColumn($set->ActualFinish, translate('Actual finish'))->setVisible(false)
        );
        $this->addColumn(\widget_TableModelViewColumn($set->Start, translate('Scheduled start'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Finish, translate('Scheduled finish'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Work, translate('Scheduled work')));
        $this->addColumn(\widget_TableModelViewColumn($set->ActualWork, translate('Actual work'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->PercentWorkComplete, translate('Percent work complete')));
        $this->addColumn(\widget_TableModelViewColumn($set->Units, translate('Units')));
        $this->addColumn(\widget_TableModelViewColumn($set->ResourceUID, translate('Resource')));
        $this->addColumn(\widget_TableModelViewColumn($set->ResponsePending, translate('Response pending')));
        $this->addColumn(\widget_TableModelViewColumn($set->BookingType, translate('Booking type'))->setVisible(true));
        
        
        return $this;
    }
    
    
    
    /**
     *
     * @param Assignment      $record
     * @param string    $fieldPath
     * @return \Widget_Displayable_Interface    The item that will be placed in the cell
     */
    protected function computeCellContent(Assignment $record, $fieldPath)
    {
        $widgets = \bab_Widgets();
        
        switch($fieldPath) {
            
            case '_edit':
                return $widgets->Link(
                    $widgets->Icon('', \Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    controller()->assignment()->edit(array('UID' => $record->UID))
                );
            
            case 'Work':
                $work = new Duration($record->getParentSet()->getInterval($record->ActualWork));
                return $widgets->Label((string) $work);
                
            case 'ActualWork':
                $actualwork = new Duration($record->getParentSet()->getInterval($record->ActualWork));
                return $widgets->Label((string) $actualwork);
            
            case 'PercentWorkComplete':
                return $widgets->Label($record->PercentWorkComplete.'%');
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}
