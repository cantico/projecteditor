<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;


/**
 * Represent a duration as text
 */
class Duration
{
    /**
     * @var string
     */
    private $intervalSpec;
    
    
    /**
     * @return \DateInterval
     */
    private $interval;
    
    public function __construct(\DateInterval $interval = null)
    {
        $this->interval = $interval;
    }
    
    /**
     * Set a posted value
     * 
     * @see DurationField
     * 
     * @param Array $value array with (month, day, hour) keys
     * 
     * @return Duration
     */
    public function setValue(Array $value)
    {
        $month = (int) $value['month'];
        $day = (int) $value['day'];
        $hour = (int) $value['hour'];
        
        if (0 === ($month + $day + $hour)) {
            return $this;
        }
        
        $spec = 'P';
        
        if ($month) {
            $spec .= $month.'M';
        }
        
        if ($day) {
            $spec .= $day.'D';
        }
        
        if ($hour) {
            $spec .= 'T'.$hour.'H';
        }
        
        $this->intervalSpec = $spec;
        $this->interval = new \DateInterval($spec);
        
        return $this;
    }
    
    /**
     * Get interval spec
     * ISO 8601
     * 
     * @return string
     */
    public function getIntervalSpec()
    {
        if (!isset($this->intervalSpec)) {
            $this->intervalSpec = storage()->utilit()->getIntervalSpec($this->interval);
        }
        
        return $this->intervalSpec;
    }
    

    /**
     * Test if the duration is empty
     * @return bool
     */
    public function isEmpty()
    {
        if (null === $this->interval) {
            return true;
        }
        
        $inter = $this->interval;
        
        $sum = $inter->y + $inter->m + $inter->d + $inter->h + $inter->i + $inter->s;
        
        if (0 === $sum) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * List of quantites backward in time
     * @return array
     */
    private function getBackwardArray()
    {
        $arr = array();
        $inter = $this->interval;
        
        if ($inter->y) {
            $arr[] = sprintf(translate('-%d year', '-%d years', $inter->y), $inter->y);
        }
        
        if ($inter->m) {
            $arr[] = sprintf(translate('-%d month', '-%d months', $inter->m), $inter->m);
        }
        
        if ($inter->d) {
            $arr[] = sprintf(translate('-%d day', '-%d days', $inter->d), $inter->d);
        }
        
        if ($inter->h) {
            $arr[] = sprintf(translate('-%d hour', '-%d hours', $inter->h), $inter->h);
        }
        
        if ($inter->i) {
            $arr[] = sprintf(translate('-%d minute', '-%d minutes', $inter->i), $inter->i);
        }
        
        if ($inter->s) {
            $arr[] = sprintf(translate('-%d second', '-%d seconds', $inter->s), $inter->s);
        }
        
        return $arr;
    }
    
    /**
     * List of quantites forward in time
     * @return array
     */
    private function getForwardArray()
    {
        $arr = array();
        $inter = $this->interval;
        
        if ($inter->y) {
            $arr[] = sprintf(translate('%d year', '%d years', $inter->y), $inter->y);
        }
        
        if ($inter->m) {
            $arr[] = sprintf(translate('%d month', '%d months', $inter->m), $inter->m);
        }
        
        if ($inter->d) {
            $arr[] = sprintf(translate('%d day', '%d days', $inter->d), $inter->d);
        }
        
        if ($inter->h) {
            $arr[] = sprintf(translate('%d hour', '%d hours', $inter->h), $inter->h);
        }
        
        if ($inter->i) {
            $arr[] = sprintf(translate('%d minute', '%d minutes', $inter->i), $inter->i);
        }
        
        if ($inter->s) {
            $arr[] = sprintf(translate('%d second', '%d seconds', $inter->s), $inter->s);
        }
        
        return $arr;
    }
    
    /**
     * @return string
     */
    public function __tostring()
    {
        if (!isset($this->interval)) {
            return '';
        }
        
        if ($this->interval->invert) {
            $arr = $this->getBackwardArray();
        } else {
            $arr = $this->getForwardArray();
        }
        
        return implode(', ', $arr);
    }
    
    /**
     * @return \Widget_Label
     */
    public function getLabel()
    {
        $widgets = \bab_Widgets();
        return $widgets->Label($this->__tostring())->addClass('duration');
    }
}
