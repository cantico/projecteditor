<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use Ovidentia\LibProject\ResourceSet;
use Ovidentia\LibProject\Resource;

class ResourceList extends \widget_TableModelView
{
    public function addDefaultColumns(ResourceSet $set)
    {
        $this->addColumn(\widget_TableModelViewColumn($set->Name, translate('Name')));
        $this->addColumn(\widget_TableModelViewColumn($set->Type, translate('Type')));
        $this->addColumn(\widget_TableModelViewColumn($set->Group, translate('Group'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->EmailAddress, translate('Email')));
        $this->addColumn(\widget_TableModelViewColumn($set->Initials, translate('Initials')));
        $this->addColumn(
            \widget_TableModelViewColumn(
                $set->MaterialLabel,
                translate('Unit of measure for the material resource')
            )
            ->setVisible(false)
        );
        $this->addColumn(\widget_TableModelViewColumn($set->CreationDate, translate('Creation date')));
        $this->addColumn(\widget_TableModelViewColumn($set->user, translate('Ovidentia user')));
        
        return $this;
    }
    
    /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param    string            $name        table field name
     * @param     \ORM_Field        $field        ORM field
     *
     * @return Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        
        switch($name) {
            case 'Name':
            case 'Type':
                return parent::handleFilterInputWidget($name, $field);
        }
        
        
    }
    
    
    /**
     *
     * @param Resource    $record
     * @param string        $fieldPath
     * @return \Widget_Item    The item that will be placed in the cell
     */
    protected function computeCellContent(Resource $record, $fieldPath)
    {
        $widgets = bab_Widgets();
    
        switch($fieldPath) {
            case 'Name':
                return $widgets->Link($record->Name, controller()->resource()->edit(array('UID' => $record->UID)));
        }
    
        return parent::computeCellContent($record, $fieldPath);
    }
}
