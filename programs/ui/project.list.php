<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\ProjectEditor;

use \Ovidentia\LibProject\ProjectSet;
use \Ovidentia\LibProject\Project;

class ProjectList extends \widget_TableModelView
{
    public function addDefaultColumns(ProjectSet $set)
    {
        $this->addColumn(\widget_TableModelViewColumn($set->Name, translate('Name')));
        $this->addColumn(\widget_TableModelViewColumn($set->Title, translate('Title')));
        $this->addColumn(\widget_TableModelViewColumn($set->Author, translate('Author'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Category, translate('Category'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Company, translate('Company'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Manager, translate('Manager')));
        $this->addColumn(\widget_TableModelViewColumn($set->StartDate, translate('Start date')));
        $this->addColumn(\widget_TableModelViewColumn($set->FinishDate, translate('Finish date')));
        $this->addColumn(\widget_TableModelViewColumn($set->Subject, translate('Subject'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->Revision, translate('Revision'))->setVisible(false));
        $this->addColumn(\widget_TableModelViewColumn($set->LastSaved, translate('Last saved'))->setVisible(false));
        $this->addColumn(
            \widget_TableModelViewColumn($set->CreationDate, translate('Creation date'))->setVisible(false)
        );
    }
    
    /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param    string            $name        table field name
     * @param     \ORM_Field        $field        ORM field
     *
     * @return Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
    
        switch($name) {
            case 'Name':
            case 'StartDate':
                return parent::handleFilterInputWidget($name, $field);
        }
    
    
    }
    
    
    
    /**
     *
     * @param Project    $record
     * @param string        $fieldPath
     * @return \Widget_Item    The item that will be placed in the cell
     */
    protected function computeCellContent(Project $record, $fieldPath)
    {
        $widgets = bab_Widgets();
        
        switch($fieldPath) {
            case 'Title':
                return $widgets->Link($record->Title, controller()->project()->display($record->uuid));
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}
