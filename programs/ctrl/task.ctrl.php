<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
*/
namespace Ovidentia\ProjectEditor\Ctrl;

use Ovidentia\ProjectEditor as PE;

class Task extends Controller
{
    
    /**
     * List tasks from one project
     * @param string $project Project UUID
     *
     */
    public function displayList($project, $filter = null)
    {
        
        $list = PE\UiLoader::taskList();
        $filterCriteria = $list->getFilterCriteria($filter);
        
        $set = PE\storage()->projectSet();
        $projectRecord = $set->get($project);
        
        if (!isset($projectRecord)) {
            throw new \bab_AccessException(sprintf(PE\translate('The project %s does not exists'), $project));
        }
        
        $page = PE\UiLoader::taskListPage($projectRecord);
        $page->setTitle(sprintf(PE\translate('Tasks of project "%s"'), $projectRecord->Title));

        $set = PE\storage()->taskSet();
        $list->addDefaultColumns($set);
        $res = $set->select($set->project->is($project)->_AND_($filterCriteria));
        $list->setDataSource($res);
        
        $filterPanel = $list->filterPanel();
        $filterPanel->addClass(\Func_Icons::ICON_LEFT_24);
        
        $page->addItem($filterPanel);
        
        return $page;
    }
    
    
    
    
    public function display($task)
    {
        
        $set = PE\storage()->taskSet();
        
        $taskRecord = $set->get($task);
        
        if (!isset($taskRecord) || !isset($taskRecord->UID)) {
            throw new \bab_AccessException(sprintf(PE\translate('The task %s does not exists'), $task));
        }
        
        $page = PE\UiLoader::taskPage($taskRecord);
        
        
        $page->addItem(PE\UiLoader::taskFullFrame($taskRecord));

        return $page;
    }
    
    
    public function edit(Array $task)
    {
        
        $projectRecord = null;
        
        $pset = PE\storage()->projectSet();
        
        $tset = PE\storage()->taskSet();
        $tset->project();
        
        if (!empty($task['project'])) {

            $projectRecord = $pset->get($task['project']);
            
            if (!isset($projectRecord)) {
                throw new \bab_AccessException(sprintf(PE\translate('The project %s does not exists'), $project));
            }
        }
        
        if (!empty($task['UID'])) {
            $taskRecord = $tset->get($task['UID']);
            $projectRecord = $taskRecord->project;
            
        } else {
            $taskRecord = $tset->newRecord();
            $taskRecord->setNewTaskDefaultValues();
        }
        
        $page = PE\UiLoader::projectPage($projectRecord);
        
        $taskRecord->setFormInputValues($task);

        $editor = PE\UiLoader::taskEditor($taskRecord);
        
        $page->addItem($editor);
        
        return $page;
    }
    
    
    /**
     * Save task
     * @param Array $task
     */
    public function save(Array $task = null)
    {
        $tset = PE\storage()->taskSet();
        
        if (!empty($task['UID'])) {
            $taskRecord = $tset->get($task['UID']);
        } else {
            $taskRecord = $tset->newRecord();
        }
        
        $taskRecord->setFormInputValues($task);
        $taskRecord->save();
        
        return true;
    }
    
    
    /**
     * Delete the task
     * @param int $task    Task UID
     */
    public function delete($project)
    {
        return false;
    }
}
