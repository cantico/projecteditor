<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
*/
namespace Ovidentia\ProjectEditor\Ctrl;

use Ovidentia\ProjectEditor as PE;
use Ovidentia\ProjectEditor\UiLoader;
use Ovidentia\LibProject\MspdiImport;

class Project extends Controller
{
    
    /**
     * Display projects list
     */
    public function displayList($filter = null)
    {
        $list = UiLoader::projectList();
        $filterCriteria = $list->getFilterCriteria($filter);

        $page = UiLoader::projectListPage();
        $page->setTitle(PE\translate('Projects'));
        
        $set = PE\storage()->projectSet();
        $list->addDefaultColumns($set);
        $res = $set->select($filterCriteria);
        $list->setDataSource($res);
        
        $filterPanel = $list->filterPanel();
        $filterPanel->addClass(\Func_Icons::ICON_LEFT_24);
        
        $page->addItem($filterPanel);
        
        return $page;
    }
    
    

    
    /**
     * Display a project
     */
    public function display($project)
    {
        $set = PE\storage()->projectSet();
        
        $projectRecord = $set->get($project);
        
        if (!isset($projectRecord)) {
            throw new \bab_AccessException(sprintf(PE\translate('The project %s does not exists'), $project));
        }
        
        $page = UiLoader::projectPage($projectRecord);
        $page->addItem(UiLoader::projectFullFrame($projectRecord));
        
        
        return $page;
    }
    
    
    /**
     * Create or edit a project
     */
    public function edit($project = null)
    {
        $set = PE\storage()->projectSet();
        
        if (isset($project)) {
            $projectRecord = $set->get($project);
        } else {
            
           
            // new project
            $projectRecord = $set->newRecord();
            $projectRecord->setNewProjectdefaultValues();
            
            
        }
        
        $page = UiLoader::page();
        $page->setTitle(PE\translate('Edit project'));
        $page->addItem(UiLoader::projectEditor($projectRecord));
        
        return $page;
    }
    
    /**
     * Save project
     * @param   array $project
     */
    public function save($project = null)
    {
        $set = PE\storage()->projectSet();
        if (!empty($project['uuid'])) {
            $projectRecord = $set->get($project['uuid']);
            $oldProject = clone $projectRecord;
        } else {
            $projectRecord = $set->newRecord();
        }
        
        unset($project['uuid']);
        
        $projectRecord->setFormInputValues($project);
        $projectRecord->save();
        
        if (isset($oldProject) && $set->isTaskUpdateRequired($oldProject, $projectRecord)) {
            $projectRecord->updateTasks();
        }
        
        return true;
    }
    
    /**
     * Import form for MSPDI xml file
     * 
     */
    public function import()
    {
        $page = UiLoader::page();
        $page->setTitle(PE\translate('Import a project'));
        $page->addItem(UiLoader::importProjectEditor());
        
        return $page;
    }
    
    /**
     * Import from MSPDI xml file
     * @param Array $import
     */
    public function saveImport(Array $import = null)
    {
        $widgets = \bab_Widgets();
        $set = PE\storage()->projectSet();
        
        $filePicker = $widgets->FilePicker();
        
        $files = $filePicker->getTemporaryFiles('file');
        
        if (!isset($files)) {
            throw new \bab_SaveErrorException(PE\translate('The file to import is mandatory'));
        }
        
        $files->rewind();
        if (!$files->valid()) {
            throw new \bab_SaveErrorException(PE\translate('The file to import is mandatory'));
        }
        
        $filepath = $files->current();
        
        $mspdi = new \DOMDocument();
        $mspdi->preserveWhiteSpace = false;
        $mspdi->load($filepath->getFilePath()->tostring());
        
        
        $project = $set->newRecord();
        $import = PE\storage()->mspdiImport($mspdi, $project);
        $import->importProject();
        
        $filePicker->cleanup();
        
        return true;
    }
    
    
    /**
     * Delete the project
     * @param string $project        Project uuid
     */
    public function delete($project)
    {
        return false;
    }
}
