<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
*/
namespace Ovidentia\ProjectEditor\Ctrl;

use Ovidentia\ProjectEditor as PE;

class Resource extends Controller
{
    
    /**
     * List ressources from one project
     * @param string $project Project UUID
     *
     */
    public function displayList($project, $filter = null)
    {
        
        $list = PE\UiLoader::resourceList();
        $filterCriteria = $list->getFilterCriteria($filter);
        
        $set = PE\storage()->projectSet();
        $projectRecord = $set->get($project);
        
        if (!$project || !isset($projectRecord)) {
            throw new \bab_AccessException(sprintf(PE\translate('The project %s does not exists'), $project));
        }
        
        $page = PE\UiLoader::resourceListPage($projectRecord);
        $page->setTitle(sprintf(PE\translate('Resources of project "%s"'), $projectRecord->Title));

        $set = PE\storage()->resourceSet();
        $list->addDefaultColumns($set);
        $res = $set->select($set->project->is($project)->_AND_($filterCriteria));
        $list->setDataSource($res);
        
        $filterPanel = $list->filterPanel();
        $filterPanel->addClass(\Func_Icons::ICON_LEFT_24);
        
        $page->addItem($filterPanel);
        
        return $page;
    }
    
    
    /**
     * @param array $resource
     */
    public function edit(Array $resource)
    {
        $rset = PE\storage()->resourceSet();
        $pset = PE\storage()->projectSet();
        
        if (!empty($resource['UID'])) {
            $resourceRecord = $rset->get($resource['UID']);
            $project = $resourceRecord->getFkPk('project');
        } else {
            $resourceRecord = $rset->newRecord();
            $project = $resource['project'];
        }
        
        $projectRecord = $pset->get($project);
        
        if (!$project || !isset($projectRecord)) {
            throw new \bab_AccessException(sprintf(PE\translate('The project %s does not exists'), $project));
        }
        
        $page = PE\UiLoader::projectPage($projectRecord);
        
        
        
        
        $resourceRecord->setFormInputValues($resource);
        
        
        $editor = PE\UiLoader::resourceEditor($resourceRecord);
        
        $page->addItem($editor);
        
        return $page;
    }
    
    
    public function save(Array $resource = null)
    {
        $rset = PE\storage()->resourceSet();
        
        if (empty($resource['UID'])) {
            $resourceRecord = $rset->newRecord();
        } else {
            $resourceRecord = $rset->get($resource['UID']);
        }
        
        $resourceRecord->setFormInputValues($resource);
        $resourceRecord->save();
        
        return true;
    }
}
