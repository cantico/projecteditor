<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
*/
namespace Ovidentia\ProjectEditor\Ctrl;

use Ovidentia\ProjectEditor as PE;
use Ovidentia\ProjectEditor\UiLoader;

class Assignment extends Controller
{
    
    
    
    /**
     * Create or edit a project
     */
    public function edit(Array $assignment = null)
    {
        $set = PE\storage()->assignmentSet();
        
        if (!empty($assignment['UID'])) {
            $assignmentRecord = $set->get($assignment['UID']);
        } else {
            
            // new project
            $assignmentRecord = $set->newRecord();
            $assignmentRecord->TaskUID = $assignment['TaskUID'];
            $assignmentRecord->project = $assignment['project'];
        }
        
        $page = UiLoader::page();
        $page->setTitle(PE\translate('Edit assignment'));
        $page->addItem(UiLoader::assignmentEditor($assignmentRecord));
        
        return $page;
    }
    
    /**
     * Save project
     * @param   array $project
     */
    public function save(Array $assignment = null)
    {
        $tset = PE\storage()->taskSet();
        $aset = PE\storage()->assignmentSet();
        if (!empty($assignment['UID'])) {
            $assignmentRecord = $aset->get($assignment['UID']);
        } else {
            $assignmentRecord = $aset->newRecord();
        }
        
        $work = UiLoader::duration();
        $assignment['Work'] = $work->setValue($assignment['Work'])->getIntervalSpec();
        $assignment['ActualWork'] = $work->setValue($assignment['ActualWork'])->getIntervalSpec();
        
        
        $assignmentRecord->setFormInputValues($assignment);
        
        // ensure task and project are set
        
        $task = $tset->get($assignmentRecord->TaskUID);
        
        if (!isset($task) || !$task->UID) {
            throw new \bab_SaveErrorException('Assignment must be associated to a task');
        }
        
        $assignmentRecord->project = $task->project;
        
        $duplicate = $task->getAssignment($assignmentRecord->ResourceUID);
        if (isset($duplicate)) {
            throw new \bab_SaveErrorException(translate('An assignment with this resource allready exists'));
        }
        
        
        $assignmentRecord->save();
        
        return true;
    }
    
    
    /**
     * Delete the project
     * @param string $project        Project uuid
     */
    public function delete($project)
    {
        return false;
    }
}
