<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\ProjectEditor\Ctrl;


/**
 * 
 */
class Gantt extends Controller
{


    public function edit()
    {
        $addon = \bab_getAddonInfosInstance('projecteditor');
        
        // redirect to editor
        
        $url = new \bab_url($GLOBALS['babInstallPath'].$addon->getRelativePath().'jQueryGantt/gantt.html');
        $url->location();
        
        
        /*
        
        $W = bab_Widgets();
        $page = $W->BabPage();

        $page->setTitle(projecteditor_translate('Project editor'));
        
        
        
        $ganttPath = $GLOBALS['babInstallPath'].$addon->getRelativePath().'jQueryGantt/';
        
        $page->addStyleSheet($addon->getStylePath().'platform.css');
        $page->addStyleSheet($ganttPath.'libs/dateField/jquery.dateField.css');
        $page->addStyleSheet($ganttPath.'gantt.css');
        //$page->addStyleSheet($ganttPath.'print.css');
        
        
        $page->addJavascriptFile($ganttPath.'libs/jquery.livequery.min.js');
        $page->addJavascriptFile($ganttPath.'libs/jquery.timers.js');
        // $page->addJavascriptFile($ganttPath.'libs/platform.js');
        $page->addJavascriptFile($addon->getTemplatePath().'platform.js'); // change some path to images
        $page->addJavascriptFile($ganttPath.'libs/date.js');
        $page->addJavascriptFile($ganttPath.'libs/i18nJs.js');
        $page->addJavascriptFile($ganttPath.'libs/dateField/jquery.dateField.js');
        $page->addJavascriptFile($ganttPath.'libs/JST/jquery.JST.js');
        
        $page->addStyleSheet($ganttPath.'libs/jquery.svg.css');
        $page->addJavascriptFile($ganttPath.'libs/jquery.svg.min.js');
        
        $page->addJavascriptFile($ganttPath.'libs/jquery.svgdom.1.8.js');
        
        $page->addJavascriptFile($ganttPath.'ganttUtilities.js');
        $page->addJavascriptFile($ganttPath.'ganttTask.js');
        
        // $page->addJavascriptFile($ganttPath.'ganttDrawerSVG.js');
        $page->addJavascriptFile($addon->getTemplatePath().'ganttDrawerSVG.js'); // change some path to images
        
        $page->addJavascriptFile($ganttPath.'ganttGridEditor.js');
        $page->addJavascriptFile($ganttPath.'ganttMaster.js');
        
        $page->addJavascriptFile($addon->getTemplatePath().'projecteditor.js');
        
        $page->addItem($W->Html(file_get_contents($addon->getTemplatePath().'projecteditor.html')));
        
        return $page;
        
        */
    }
}
