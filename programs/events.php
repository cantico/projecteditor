<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\ProjectEditor;

/**
 * before sitemap creation
 * registered on this event to add more nodes to core sitemap
 *
 * @param	bab_eventBeforeSiteMapCreated 	$event
 */
function onBeforeSiteMapCreated(\bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';
    require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
    
    \bab_functionality::includeOriginal('Icons');
    $delegations = \bab_getUserSitemapDelegations();

    foreach ($delegations as $key => $deleg) {
        $position = array('root', $key, 'babUser', 'babUserSection');

        $item = $event->createItem('projecteditorUser');

        $item->setLabel(translate('Projects'));
        $item->setLink(controller()->project()->displayList()->url());
        $item->addIconClassname(\Func_Icons::APPS_TASK_MANAGER);
        $item->setPosition($position);

        $event->addFunction($item);
    }
}
